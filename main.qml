import Qt 4.7
import QtMultimediaKit 1.1

Rectangle {
	SoundEffect {
		id: click
		source: "sfx/click.wav"
	}

	SoundEffect {
		id: shoot
		source: "sfx/shoot.wav"
	}

	SoundEffect {
		id: spin
		source: "sfx/spin.wav"
	}

	id: main
	width: 480
	height: 800

	Image {
		id: background
		source: "background.png"
	}

	Image {
		x: 45		// offset
		y: 270
		id: trigger
		source: "gun/trigger.png"
	}

	Image {
		x: 325		// offset
		y: 115
		id: hane
		source: "gun/hane.png"
	}

	Image {
		x: -41		// offset
		y: -106
		id: revolver
		source: "gun/gun1.png"
	}

	/*
		Cylinder code
	*/
	property int cylinderTicks : 0		// for timer
	Timer {
		id: cylinderTimer
		interval: 20
		running: false
		repeat: true
		onTriggered: {
			cylinderTicks++
			rotateCylinder();
		}
	}

	function rotateCylinder() {
		if(cylinderTicks > 110)
			cylinderTimer.stop()

		state = cylinderTicks % 4;
		// DRY: how dirty
		if(state == 0) {
			cylinderState1.visible = true;
			cylinderState4.visible = false;
		}
		if(state == 1) {
			cylinderState2.visible = true;
			cylinderState1.visible = false;
		}
		if(state == 2) {
			cylinderState3.visible = true;
			cylinderState2.visible = false;
		}
		if(state == 3) {
			cylinderState4.visible = true;
			cylinderState3.visible = false;
		}
	}

	property int bullets : 6		// chamber
	property int currentChamber : 0
	property variant chamber : [0, 0, 0, 0, 0, 1]

	Image {
		id: cylinderState1
		x: 252
		y: 316
		source: "barrel/barrel1.png"
	}

	Image {
		id: cylinderState2
		x: 252
		y: 316
		source: "barrel/barrel2.png"
		visible: false
	}

	Image {
		id: cylinderState3
		x: 252
		y: 316
		source: "barrel/barrel3.png"
		visible: false
	}

	Image {
		id: cylinderState4
		x: 252
		y: 316
		source: "barrel/barrel4.png"
		visible: false
	}

	function onCylinderSwipe(offset) {
		//cylinderTimer.stop()
		if(!cylinderTimer.enabled && (offset < 0)) { // direction
			cylinderTicks = 0
			cylinderTimer.start();
			currentChamber = (Math.abs(offset) % bullets)	// new current chamber
			spin.play()
		}
	}

	MouseArea {
		id: cylinderMouse
		x: 152
		y: 316
		width: 306
		height: 239

		property int oldX: 0
		property int oldY: 0

		onPressed: {
			oldX = mouseX;
			oldY = mouseY;
		}

		onReleased: {
			var xDiff = oldX - mouseX;
			var yDiff = oldY - mouseY;

			if(Math.abs(xDiff) > Math.abs(yDiff)) {
				onCylinderSwipe(yDiff);
			}
		}
	}

	/*
		Trigger code
	*/
	function moveTrigger(offset) {
		// TODO:
		console.log("Trigger " + offset);
	}

	function onTriggerSwipe(offset) {
		if(offset < 0) {
			moveTrigger(offset);
			if(chamber[currentChamber] == 0) {
				click.play();
			} else {
				shoot.play()
			}
			currentChamber = (++currentChamber % bullets)	// make sure we never have more
			console.log(currentChamber)
		}
	}

	MouseArea {
		id: triggerMouse
		x: 30
		y: 260
		width: 180
		height: 240

		property int oldX: 0
		property int oldY: 0

		onPressed: {
			oldX = mouseX;
			oldY = mouseY;
		}

		onReleased: {
			var xDiff = oldX - mouseX;
			var yDiff = oldY - mouseY;

			if(Math.abs(xDiff) < Math.abs(yDiff)) {
				onTriggerSwipe(xDiff);
			}
		}
	}

}

